# Delftse Foundations of Computation

Source files for the open textbook *Delftse Foundations of Computation*. This book is a [Jupyter book](https://jupyterbook.org/intro.html). To build it, install the packages in requirements.txt, then run `jb build ./`

## Authors and acknowledgment
This book was written by Stefan Hugtenburg & Neil Yorke-Smith.

## License
This book is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.