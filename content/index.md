Delftse Foundations of Computation
=================================

**<span style="font-variant: small-caps;"><span style="color: #00A6D6;"><b>Delftse</span> Foundations of Computation</b></span>**
is a textbook for a one-quarter introductory course in theoretical computer science.  It includes topics from propositional and predicate logic, proof techniques, discrete structures, set theory and the theory of computation, along with practical applications to computer science.  It has no prerequisites other than a general familiarity with computer programming.



This book is derived from *Foundations of Computation* by Carol Critchlow and David Eck, Version 2.3 (Summer 2011), which is licensed under CC BY-NC-SA 4.0. Critchlow and Eck are not associated with the TU Delft editions. This book also uses some material from Wikipedia (English) (en.wikipedia.org), which is licensed under CC BY-SA 3.0. The authors of the TU Delft editions are responsible for any errors, and welcome bug reports and suggestions by email or in person.



Thanks to M.&nbsp;de&nbsp;Jong, T.&nbsp;Klos, I.&nbsp;van&nbsp;Kreveld, F.&nbsp;Mulder, H.&nbsp;Tonino, E.&nbsp;Walraven, and all students who reported bugs or provided exercise solutions.

Thanks to M.~Noordsij and the Interactive Textbook project for converting this book to an interactive format which you are seeing here!


This work can be redistributed in unmodified form, or in modified form with proper attribution and under the same licence as the original, for non-commercial uses only, as specified by the *Creative Commons Attribution-Non-commercial-ShareAlike 4.0 Licence* (creativecommons.org/licenses/by-nc-sa/4.0/).



The latest edition of this book is available for online use and for free download from the TU Delft Open Textbook repository at textbooks.open.tudelft.nl. The original (non-TU Delft) version contains additional material, and is available at math.hws.edu/FoundationsOfComputation/.
