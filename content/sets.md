
(chapter:set-theory)=
# Sets, Functions, and Relations

<span style="font-variant: small-caps;">We deal with the complexity</span> of the world by putting
things into categories.  There are not just hordes of individual
creatures.  There are dogs, cats, elephants and mice.  There are
mammals, insects and fish.  Animals, vegetables and minerals.
Solids, liquids and gases.  Things that are red.  Big cities.
Pleasant memories... .  Categories build on categories.  They are the
subject and the substance of thought.

```{index} sets
```
In mathematics, which operates in its own abstract and rigorous world,
categories are modelled by sets.  A set is just a collection of
elements.  Along with logic, sets form the 'foundation' of
mathematics, just as categories are part of the foundation of
day-to-day thought.  In this chapter, we study sets and relationships
among sets.  And, yes, that means we'll prove theorems about sets!

