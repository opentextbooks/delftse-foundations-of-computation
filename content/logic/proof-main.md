
(chapter:proof)=
# Proof

```{index} formal proof
```

<span style="font-variant: small-caps;">Mathematics is unique</span> in that it claims a certainty
that is beyond all possible doubt or argument.  A mathematical proof
shows how some result follows by logic alone from a given set of
assumptions, and once the result has been proven, it is as solid as
the foundations of logic themselves.
Of course, mathematics achieves this certainty by restricting itself
to an artificial, mathematical world, and its application to the
real world does not carry the same degree of certainty.

Within the world of mathematics, consequences follow from assumptions
with the force of logic, and a proof is just a way of pointing out
logical consequences.  

Of course, the fact that mathematical results follow logically
does not mean that they are obvious in any normal sense.  Proofs are
convincing once they are discovered, but finding them is often
very difficult.  They are written in a language and style
that can seem obscure to the uninitiated.
Often, a proof builds on a long series of definitions
and previous results, and while each step along the way might be
'obvious' the end result can be surprising and powerful.
This is what makes the search for proofs worthwhile.

In this chapter, we'll look at some approaches and techniques
that can be used for proving mathematical results, including two
important proof techniques known as proof by contradiction and
mathematical induction.  Along the way, we'll encounter a few new
definitions and notations.  Hopefully, you will be left with
a higher level of confidence for exploring the mathematical world
on your own.

