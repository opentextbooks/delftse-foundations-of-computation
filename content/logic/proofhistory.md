(sec:the_historical_background)=
## A Little Historical Background

```{index} proof
```

The mathematical world and the real world weren't always quite so
separate.  Until around the middle of the nineteenth
century, the statements of mathematics were regarded as
statements about the world.  A proof was simply a convincing
argument, rather than a chain forged of absolute logic.  
It was something closer to the original meaning of the word 'proof',
as a test or trial: you might have heard of the proverb, ''The proof of the pudding is in the eating.''   So, to prove something was to test its truth by
putting it to the trial of logical argument.

```{warning}
A commonly made mistake centres around the difference between 'proof' (a noun) and 'to prove' (a verb). You can prove
a certain claim using a proof. But grammatically speaking you cannot proof a certain claim using a prove.
Historically, in the course *Reasoning \& Logic* this is one of the most common spelling/grammar mistakes on
exams. By including it in this book, we hope that you will now know better.

```

```{index} Euclid, non-Euclidean geometry
```
The first rumble of trouble came in the form of non-Euclidean
geometry.  For two thousand years, the geometry of the Greek mathematician
Euclid had been accepted, simply, as the geometry of the world.
In the middle of the nineteenth century, it was discovered that
there are other systems of geometry, which are at least as valid
and self-consistent as Euclid's system.  Mathematicians can work
in any of these systems, but they cannot all claim to be working 
in the real world.

```{index} Frege - Gottlob, Russell - Bertrand, Russell's Paradox
```
Near the end of the nineteenth century came another shock, in the form
of cracks in the very foundation of mathematics.  At that time,
mathematician Gottlob Frege was finishing
a book on set theory that represented his life's work.  In Frege's
set theory, a set could be defined by any property.  You could have,
for example, the set consisting of all sets that contain three objects.
As he was finishing his book, Frege received a letter from a young
mathematician named Bertrand Russell which
described what became known as Russell's Paradox.  Russell
pointed out that the set of all sets---that is, the set that contains
every entity that satisfies the property of being a set---cannot logically
exist.  We'll see Russell's reasoning in the following chapter.  Frege could
only include a postscript in his book stating that the basis of the
work had been swept away.

```{index} Peirce - Charles, Frege - Gottlob
:name: person:frege
```

````{admonition} Person 

```{figure} ../figures/people/frege.jpg 
:width: 50%
:align: center
```
A contemporary of Peirce (see  [this section](person:peirce)), Friedrich Ludwig Gottlob Frege (1848--1925) was a German philosopher, logician, and mathematician.  Peirce and Frege were apparently mostly unaware of each other's work.  Frege is understood by many to be the father of analytic philosophy, concentrating on the philosophy of language and mathematics. Like Peirce, his work was largely ignored during his lifetime, and came to be recognised by later mathematicans---including Russell.

Source: en.wikipedia.org/wiki/Gottlob_Frege.

````

```{index} Godel - Kurt
```

Mathematicians responded to these problems by banishing appeals to
facts about the real world from mathematical proof.  Mathematics was to
be its own world, built on its own secure foundation.  The foundation
would be a basic set of assumptions or 'axioms' from which
everything else would follow by logic.  It would only be
necessary to show that the axioms themselves were logically consistent and complete,
and the world of mathematics would be secure.  Unfortunately,
even this was not to be.  In the 1930s, Kurt G&ouml;del
showed that there is no consistent, finite set of axioms that completely
describes even the corner of the mathematical world known as
arithmetic.  G&ouml;del showed that given any finite, consistent set of
axioms, there are true statements about arithmetic that do not follow
logically from those axioms.  We will return to G&ouml;del and his contemporaries in {numref}`chapter:conc`.

We are left with a mathematical world in which iron chains of
logic still bind conclusions to assumptions.  But the assumptions
are no longer rooted in the real world.  Nor is there any finite core
of axioms to which the rest of the mathematical world can be chained.
In this world, axioms are set up as signposts in a void, and then
structures of logic are built around them.  
For example, in the next chapter, instead of talking about
*the* set theory that describes the real world, we have *a*
set theory, based on a given set of axioms.  That set theory is
necessarily incomplete, and it might differ from other set theories
which are based on other sets of axioms.
