
# Further Reading
The books by Christian \& Griffiths, Hofstadter and Smullyan are on recreational maths.  Hofstadter's is a winner of the Pulitzer Prize.  Besides Dowek, the other titles are textbooks; Hammack's book is an Open Textbook, just like this book is.

1.  Christian, B. and Griffiths, T. (2016).  *Algorithms to Live By: The Computer Science of Human Decisions*.  New York, NY: Henry Holt and Co.

1.  Critchlow, C. and Eck, D. (2011).  *Foundations of Computation*, version 2.3.1.  Geneva, NY: Hobart and William Smith Colleges.  math.hws.edu/FoundationsOfComputation/

1.  Dowek, G. (2015).  *Computation, Proof, Machine: Mathematics Enters a New Age*.  New York, NY: Cambridge University Press.  Original in French: *Les M\'etamorphoses du calcul* (2007).

1.  Epp, S. S. (2019).  *Discrete Mathematics with Applications*, 5th edition.  Boston, MA: Cengage Learning.

1.  Grassmann, W. K. and Tremblay, J.-P. (1996).  *Logic and Discrete Mathematics*.  Upper Saddle River, NJ: Prentice-Hall.

1.  Hammack, R. (2018).  *Book of Proof*, 3rd edition.  Richmond, VA: Virginia Commonwealth University.  www.people.vcu.edu/~rhammack/BookofProof3/

1.  Hofstadter, D. (1979).  *G&ouml;del, Escher, Bach: An Eternal Golden Braid*.  New York, NY: Basic Books.

1.  Huth, M. and Ryan, M. (2004).  *Logic in Computer Science*, 2nd edition.  Cambridge, UK: Cambridge University Press.

1.  Smullyan, R. M. (1979).  *What is the Name of This Book?* Upper Saddle River, NJ: Prentice-Hall.

