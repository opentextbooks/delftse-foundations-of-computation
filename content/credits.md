# About the Authors
```{figure} figures/people/Hugtenburg.jpg
---
width: 150
align: right
---
Stefan Hugtenburg
```
Stefan Hugtenburg holds a MSc in Computer Science from the Delft University of Technology, where he now teaches in the undergraduate Computer Science and Engineering programme. He is involved in all courses of the Algorithmics track in the curriculum, starting with this book and the course Reasoning & Logic, up until the final year course Complexity Theory.

```{figure} figures/people/Yorke-Smith.jpg
---
width: 150
align: right
---
Neil Yorke-Smith
```
Neil Yorke-Smith is an Associate Professor of Algorithmics in the Faculty of Electrical Engineering, Mathematics and Computer Science at the Delft University of Technology. His research focuses on intelligent decision making in complex socio-technical situations, with a particular current interest in agent-based methodologies and behavioural factors in automated planning and scheduling. He teaches Reasoning & Logic and graduate courses in Artificial Intelligence.


