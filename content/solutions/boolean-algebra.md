## Solutions Boolean Algebra
Solutions to {numref}`S-logic-2`

```{solution-start} exercise11
```
It is not. Take for example $p=q=0$. In this case $\lnot(p \leftrightarrow q)$ is false, but $(\lnot p) \leftrightarrow (\lnot q)$ is true. Try to see if you can find a more simplified expression that $(\lnot p) \leftrightarrow (\lnot q)$ is equivalent to.


```{solution-end}
```

```{solution-start} exercise12
```
Verify the following answers with truth tables yourself!
1. $q \rightarrow p$
1. $\mathbb{F}$
1. $\lnot p$
1. $\lnot p \wedge q$
1. $\mathbb{T}$
1. $q$


```{solution-end}
```

```{solution-start} exercise13
```
When translating try make the English sentences flow a bit without adding in more constraints. For example by using the word 'but' rather than 'and' in two of the examples below.
1. It is not sunny or it is not cold.
1. I will have neither stroopwafels nor appeltaart.
1. It is Tuesday today, but this is not Belgium.
1. You passed the final exam, but you did not pass the course.


```{solution-end}
```

