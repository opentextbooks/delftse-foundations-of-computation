```{tip} 
Couldn't find your answer here? Feel free to submit your own for future editions of the book here:
https://gitlab.ewi.tudelft.nl/reasoning_and_logic/book_solutions. We will add your name to the list of
contributors for the book if we accept your answers!
```

### Contributors to Solutions

*Max van Deursen*

*Kevin Chong*

*Julian Kuipers*

*Pia Keukeleire*

*Philippos Boon Alexaki*

*Thijs Schipper*
