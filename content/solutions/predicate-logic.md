## Solutions Predicate Logic
Solutions to {numref}`S-logic-4`

```{solution-start} exercise14
```
1. $\exists x (P(x))$
1. $\forall x(\lnot P(x)\vee \lnot Q(x))$
1. $\exists z(P(z) \wedge \lnot Q(z))$
1. $(\exists x (\lnot P(x)))\vee (\exists y (\lnot Q(y)))$
1. $\exists x \forall y \lnot P(x,y)$
1. $\forall x (\lnot R(x)\vee \exists y \lnot S(x,y))$
1. $\forall y((\lnot P(y)\wedge Q(y)) \vee (P(y) \wedge \lnot Q(y)))$
1. $\exists x (P(x) \wedge (\forall y \lnot Q(x,y)))$


```{solution-end}
```

```{solution-start} exercise15
```
We use the predicates $ Ball (x)$ for $x$ is a ball and $ Have (x,y)$ for $x$ must have $y$. We also use a constant $ you $ to represent you.
$
\exists x (*Ball*(x) \wedge \forall y (x \neq y \rightarrow \lnot *Ball*(y)) \wedge *Have*(*you*, x))
$


```{solution-end}
```

```{solution-start} exercise16
```
Using the predicates $ Person (x)$ for $x$ is a person, $ Question (x)$ for $x$ is a question, $ Answer (x)$ for $x$ is an answer, and $ Has (x,y)$ for $x$ has $y$.
Two different interpretations are:
$
\exists x (*Person*(x) \wedge \forall y (*Question*(y) \rightarrow (\exists z (*Answer*(z) \wedge *Has*(x,z)))))
$
In other words, there is a single person who has the answers to all questions. 
$
\forall x (*Question*(x) \rightarrow \exists y \exists z (*Person*(y) \wedge *Answer*(z) \wedge *Has*(y,z)))
$
In other words, every question has an answer and some person knows it (but different people might know the answer to different questions).


```{solution-end}
```

